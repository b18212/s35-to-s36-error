//! import the Task model in our controllers.
//So that our controllers functions may have access to our Task model
const Task = require("../models/Task.js");

//! Create task
// end goal: create task without
module.exports.createTaskController = (req, res) => {
    console.log(req.body);

    // mongodb: db.tasks.findOne({name: "nameOfTask"})
    // The findOne() function is used to find one document according to the condition. If multiple documents match the condition, then it returns the first document satisfying the condition.
    // then(anonymous function)
    Task.findOne({ name: req.body.name })
        .then((result) => {
            // checking the captured data from .then()
            console.log(result);
            // expected output:
            /*{
        _ObjectId: 0456f41a54as4f1ds32h1r5,
        name: "nameOfTask",
        status: "pending"
    }
    */

            if (result !== null && result.name === req.body.name) {
                return res.send("Duplicate task found");
            } else {
                let newTask = new Task({
                    name: req.body.name,
                    status: req.body.status,
                });

                newTask
                    .save()
                    .then((result) => res.send(result))
                    .catch((error) => res.send(error));
            }
        })
        .catch((error) => res.send(error));
};

// !Retrieve ALL Tasks

module.exports.getAllTaskController = (req, res) => {
    // similar: db.tasks.find({})
    Task.find({})
        .then((result) => res.send(result))
        .catch((error) => res.send(error));
};

// !retrieval single task
module.exports.getSingleTaskController = (req, res) => {
    console.log(req.params);

    // db.tasks.findOne({_id: "id"})
    Task.findById(req.params.id)
        .then((result) => res.send(result))
        .catch((error) => res.send(error));
};

// !Updating Task Status
module.exports.updateTaskStatusController = (req, res) => {
    console.log(req.params.id);
    console.log(req.body);

    let updates = {
        status: req.body.status,
    };

    //! findByIdAndUpdate
    // * 3 arguments
    // a. where will you get the id? req.params.id
    // b. what is the update variable
    // c. optional {new: true} - return the updated version of the document we are updating

    Task.findByIdAndUpdate(req.params.id, updates, { new: true })
        .then((updatedTask) => res.send(updatedTask))
        .catch((err) => res.send(err));
};