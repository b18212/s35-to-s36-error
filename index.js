//!required modules
const express = require("express");

//*Mongoose
// is a package that uses an ODM or object document mapper. It allows us to translate our JS objects into database documents for MongoDB. It allows connection and easier manipulation of our documents in MongoDB

const mongoose = require("mongoose");

//!Port
const port = 4000;

//!Server
const app = express();

//!mongoose connection

//* mongoose.connect
//is the method to connect your API to your mongoDB via the use of mongoose. It has 2 arguments:

//? First, is the connection string to connect our api to our mongodb atlas.

//? Second, is an object used to add information between mongoose and mongodb.

//? replace/change <password> in the connection string to your db password

//? add database name to task182

//*MongoDB
//upon connection and creating our first documents will create the task182 database for us.

//* Syntax:
//? mongoose.connect("<connectionStringFromMongoDBAtlas>", {
//? 	useNewUrlParser: true,
//? 	useUnifiedTopology: true
//? })

mongoose.connect(
    "mongodb+srv://admin:admin@wdc028-course-booking.jck1e.mongodb.net/tasks182?retryWrites=true&w=majority", {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    }
);
// this will create a notification if the db connection is succesful or not
let db = mongoose.connection;

// * once vs on:
//? ONCE
//signifies that the event will be called only once i.e the first time the event occurred like here in this case the first time when the connection is opened ,it will not occur once per request but rather once when the mongoose connection is made with the db
//? ON
// signifies the event will be called every time that it occurred
// We add this so that when db has a connection error, we will show the connection error both in the terminal and in the browser for our client
db.on("error", console.error.bind(console, "DB Connection Error"));

//Once the connection is open and successful, we will output a message in the terminal
db.once("open", () => console.log("Successfully connected to MongoDB"));

//!middleware
app.use(express.json());

//! reading of data forms
// usually string or array are being accepted, with this middleware, this will enable us to accept other data types
app.use(express.urlencoded({ extended: true }));

//!Group Routes
const taskRoutes = require("./routes/taskRoutes");
app.use("/tasks", taskRoutes);

//!Port listener
app.listen(port, () => console.log(`Server is running at port ${port}`));